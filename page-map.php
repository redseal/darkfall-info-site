<?php
/*
Template Name: DF Map
*/
global $ver;
get_header();
?>
<link rel="stylesheet" href="http://wiki.riseofagon.info/map/styles/main.css?t=1494013443615">
<script src="http://wiki.riseofagon.info/map/scripts/vendor/modernizr.js?t=1494013443615"></script>
<script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
<script>
  var googletag = googletag || {};
  googletag.cmd = googletag.cmd || [];
</script>

<script>
  googletag.cmd.push(function() {
    googletag.defineSlot('/102369011/dfinfo-rightColumn', [120, 600], 'div-gpt-ad-1475822058758-0').addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.pubads().collapseEmptyDivs();
    googletag.enableServices();
  });
</script>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyARf2g0lsBeq2bgOpGHP3h1uY_O-oESrj0"></script>
<article id="content" class='full-width map'>
  <div class="dfad right">
      <!-- /102369011/dfinfo-rightColumn -->
      <div id="div-gpt-ad-1475822058758-0" style="height:600px; width:120px">
        <script>
        googletag.cmd.push(function() { googletag.display('div-gpt-ad-1475822058758-0'); });
               setInterval(function(){googletag.pubads().refresh();}, 60000);
        </script>
      </div>
    </div>
    <div class="sidebar">
      <div class="beta">
        Beta 0.3.2
      </div>
      <nav>
        <span class="glyphicon glyphicon-remove sidebar-close"></span> <span class="glyphicon glyphicon-menu-hamburger sidebar-open"></span>
      </nav>
      <div class="input-group sidebar-search">
        <span class="input-group-addon" id="search-box"><span class="glyphicon glyphicon-search"></span></span> <input aria-describedby="search-box" class="form-control" name="search" placeholder="Search for..." type="text">
      </div>
      <div class="sidebar-filters">
        <div class="input-group autoshow">
          <input checked="checked" id="autoshow" type="checkbox"><label for="autoshow">Auto show labels</label>
        </div>
        <section>
          <h4 class="mobs"><span class="glyphicon glyphicon-unchecked"></span> Mobs</h4>
          <div class="checkbox">
            <label><input data-type="mobs-easy" type="checkbox"> Easy</label>
          </div>
          <div class="checkbox">
            <label><input data-type="mobs-medium" type="checkbox"> Medium</label>
          </div>
          <div class="checkbox">
            <label><input data-type="mobs-hard" type="checkbox"> Hard</label>
          </div>
          <div class="checkbox">
            <label><input data-type="mobs-boss" type="checkbox"> Boss</label>
          </div>
        </section>
        <section>
          <h4>Player Living</h4>
          <div class="checkbox">
            <label><input data-type="banks" type="checkbox"> Banks</label>
          </div>
          <div class="checkbox">
            <label><input data-type="holdings" type="checkbox"> Holdings</label>
          </div>
        </section>
        <section>
          <h4>Other</h4>
          <div class="checkbox">
            <label><input data-type="portals" type="checkbox"> Portal Network</label>
          </div>
          <div class="checkbox">
            <label><input data-type="dungeons" type="checkbox"> Dungeons</label>
          </div>
          <div class="checkbox">
            <label><input data-type="pois" type="checkbox"> Misc</label>
          </div>
        </section>
      </div>
      <div class="sidebar-details">
        <div class="sidebar-details-content"></div><button class="btn btn-xs btn-warning" id="report"><span class="glyphicon glyphicon-warning-sign"></span> Report an issue</button>
      </div>
    </div>
    <div id="map">
      <span class="loading">loading tiles...</span>
    </div>
    <div class="modal fade" id="feedbackForm" role="dialog" tabindex="-1">
      <form action="http://wiki.riseofagon.info/map/mailer.php" id="ajax-contact" method="post" name="ajax-contact">
        <input name="version" type="hidden" value="">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Feedback</h4>
            </div>
            <div class="modal-body container-fluid">
              <div class="row">
                <div class="form-group col-xs-6">
                  <label for="name">Name</label><input class="form-control" id="name" name="name" placeholder="Name" required="" type="text">
                </div>
                <div class="form-group col-xs-6">
                  <label for="email">Email address</label><input class="form-control" id="email" name="email" placeholder="Email" required="" type="email">
                </div>
              </div>
              <div class="row">
                <div class="form-group col-xs-6">
                  <label for="subject">Subject</label><select class="form-control" id="subject" name="subject">
                    <option selected="selected" value="other">
                      Other
                    </option>
                    <option value="addition">
                      POI Addition
                    </option>
                    <option value="incorrect">
                      Incorrect Info
                    </option>
                    <option value="question">
                      Question?
                    </option>
                  </select>
                </div>
                <div class="form-group col-xs-6 pidContainer">
                  <label for="poiNum">POI #</label>
                  <div class="form-group input-group">
                    <span class="input-group-addon" id="basic-addon3">PID #:</span> <input aria-describedby="basic-addon3" class="form-control" disabled="disabled" id="poiNum" name="poiNum" type="text">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="message">Message:</label>
                <textarea class="form-control" id="message" name="message" required="" rows="3">You guys rock! You simply are amazing and I just wanna...</textarea>
              </div>
              <div class="form-group poiLocContainer">
                <label for="poiLoc">POI Location</label><input aria-describedby="basic-addon3" class="form-control" disabled="disabled" id="poiLoc" name="poiLoc" type="text">
              </div>
            </div>
            <div class="modal-footer">
              <div class="label" id="form-messages"></div><!-- <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button> --><button class="btn btn-primary" type="submit">Submit</button>
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </form>
    </div><!-- /.modal -->
</article>
<script src="http://wiki.riseofagon.info/map/scripts/vendor.js?t=1494013443615"></script>
<script src="http://wiki.riseofagon.info/map/scripts/plugins.js?t=1494013443615"></script>
<script src="http://wiki.riseofagon.info/map/scripts/main.js?t=1494013443615"></script>
<?php get_footer(); ?>
