<?php
/*
Template Name: Misc Game Info
*/
get_header();
wp_enqueue_script('dfuw_table_sort', get_template_directory_uri().'/js/jquery.tablesorter.min.js',false,$ver,'all');
?>
<article id="content" class="misc-game-info">
<?php the_post(); ?>
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<h1 class="entry-title"><?php the_title(); ?></h1>
<div class="entry-content">
  <p  class='tip'><i class="icon-info-sign"></i> Table columns are sortable</p>
  <div class='misc-info left'>
    <h3>Weapons Reaches, Arcs and speeds </h3>
    <table cellspacing="0" cellpadding="2" border="1">
      <thead>
        <tr>
          <th>Type <i class="icon-chevron-up icon-white"></i></th>
          <th>Reach <i class="icon-chevron-up icon-white"></i></th>
          <th>Arc <i class="icon-chevron-up icon-white"></i></th>
          <th>Speed <i class="icon-chevron-up icon-white"></i></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Axe</td>
          <td>2.1</td>
          <td>60&deg;</td>
          <td>.50</td>
        </tr>
        <tr>
          <td>Club</td>
          <td>2.1</td>
          <td>60&deg;</td>
          <td>0.5</td>
        </tr>
        <tr>
          <td>Knives</td>
          <td>1.8</td>
          <td>80&deg;</td>
          <td>0.9</td>
        </tr>
        <tr>
          <td>Greataxe</td>
          <td>2.5</td>
          <td>60&deg;</td>
          <td>.25/.3/.35</td>
        </tr>
        <tr>
          <td>Greatclub</td>
          <td>2.5</td>
          <td>60&deg;</td>
          <td>.2/.3/.35</td>
        </tr>
        <tr>
          <td>Greatsword</td>
          <td>2.5</td>
          <td>60&deg;</td>
          <td>.35/.4/.45</td>
        </tr>
        <tr>
          <td>Polearm</td>
          <td>2.6</td>
          <td>80&deg;</td>
          <td>05/.1/.15</td>
        </tr>
        <tr>
          <td>Sword</td>
          <td>2.1</td>
          <td>60&deg;</td>
          <td>.6</td>
        </tr>
        <tr>
          <td>Sithras</td>
          <td>2.2</td>
          <td>??</td>
          <td>.3</td>
        </tr>
      </tbody>
    </table>
  </div>
  <div class="misc-info right">
    <h3>Racial and Gender reaches </h3>
    <table cellspacing="0" cellpadding="2" border="1">
      <thead>
        <tr>
          <th>Race <i class="icon-chevron-up icon-white"></i></th>
          <th>Male <i class="icon-chevron-up icon-white"></i></th>
          <th>Female <i class="icon-chevron-up icon-white"></i></th>
          </tr>
      </thead>
      <tbody>
        <tr>
          <td>Alfar</td>
          <td>2.48</td>
          <td>2.46</td>
        </tr>
        <tr>
          <td>Dwarf</td>
          <td>2.5</td>
          <td>2.48</td>
        </tr>
        <tr>
          <td>Human</td>
          <td>2.52</td>
          <td>2.48</td>
        </tr>
        <tr>
          <td>Mahirim</td>
          <td>2.639</td>
          <td>2.613</td>
        </tr>
        <tr>
          <td>Mirdain</td>
          <td>2.544</td>
          <td>2.544</td>
        </tr>
        <tr>
          <td>Orkish</td>
          <td>2.585</td>
          <td>2.577</td>
        </tr>
      </tbody>
    </table>
  </div>
  <br>
  <div class="misc-info full">
      <h3>Front Loaded Stat Value</h3>
      <img src="<?php echo get_template_directory_uri(); ?>/images/bKmsOfO.width-800.png" />
    </div>
     <div class="misc-info full">
      <h3>Front Loaded Skill Value</h3>
      <img src="<?php echo get_template_directory_uri(); ?>/images/4R5QavM.width-800.png" />
    </div>
    <br>
</div>
</div>
</article>
<?php get_sidebar(); ?>
<?php get_footer(); ?>asda
