# Darkfall Unholy Wars WP Theme

This is the repo for the Darkfall: UW Wordpress Theme

## Contributers

- [Ragnarokdel](http://twitter.com/Ragnarokdel)
- [RedSeal](http://twitter.com/iamredseal)

### About

Rag and Red made this wordpress theme because there was not alot of information on the game at the time. We put a lot of hours into designing, developing, and capturing all the data. We made this site to be a hub with reliable information for the community.

### What Now

Over time, we both were not involved in the community as much anymore. And this site sat. Sat and sat and sat. So, we now open it up and let anyone that wants to clone it, can and use as you wish.

## So How Do I Use This?

Well, you first need to clone this repo. Install WordPress and add this cloned repo as a folder in your theme directory for WordPress.

Within this repo, you should remove the database `dfuw_wp.sql` and import it into the db that your WP install created.

After that, you _should_ be all set. Rag and Red might be reachable to assist, but we are no longer supporting this project, and you might have to figure this all out on your own.
